import 'dart:core' as core;

import 'package:field_delegate/field_delegate.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shared_preferences_field_delegate/src/shared_pref.dart';
import 'package:shared_preferences_field_delegate/src/types/bool_shared_preferences_field.dart';
import 'package:shared_preferences_field_delegate/src/types/custom_model_shared_preferences_field.dart';
import 'package:shared_preferences_field_delegate/src/types/double_shared_preferences_field.dart';
import 'package:shared_preferences_field_delegate/src/types/enum_shared_preferences_field.dart';
import 'package:shared_preferences_field_delegate/src/types/int_shared_preferences_field.dart';
import 'package:shared_preferences_field_delegate/src/types/json_map_shared_preferences_field.dart';
import 'package:shared_preferences_field_delegate/src/types/string_list_shared_preferences_field.dart';
import 'package:shared_preferences_field_delegate/src/types/string_shared_preferences_field.dart';

/// Used to create instances of [SharedPreferencesField]
/// Save instance of this factory and call [dispose] to close all instances of [Field]
///
/// If you have added implementation of [SharedPreferencesField] with custom type,
/// you can extend [SharedPreferencesFieldFactory] to add a method that creates an instance of this field
/// and store that instance with [saveFieldToDispose] method to dispose later
class SharedPreferencesFieldFactory {
  final SharedPreferences _sharedPreferences;
  final _fields = <SharedPreferencesField>{};

  SharedPreferencesFieldFactory(
    this._sharedPreferences,
  );

  Field<core.bool?> bool(core.String key) =>
      saveFieldToDispose(BoolSharedPreferencesField(
        key,
        _sharedPreferences,
      ));

  Field<T?> customModel<T>(
    core.String key, {
    required ModelToJsonConverter<T> modelToJson,
    required JsonToModelConverter<T> jsonToModel,
  }) =>
      saveFieldToDispose(CustomModelSharedPreferencesField<T>(
        key,
        _sharedPreferences,
        modelToJson,
        jsonToModel,
      ));

  Field<core.double?> double(core.String key) =>
      saveFieldToDispose(DoubleSharedPreferencesField(
        key,
        _sharedPreferences,
      ));

  /// Allows to create a [SharedPreferencesField] with [Enum] type
  /// [enumValues] — values list of a [Enum]
  ///
  /// You can get a list of values of [Enum] by calling static [Enum.values] getter
  Field<T?> enumValue<T extends core.Enum>(
    core.String key,
    core.Iterable<T> enumValues,
  ) =>
      saveFieldToDispose(EnumSharedPreferencesField(
        key,
        _sharedPreferences,
        enumValues,
      ));

  Field<core.int?> int(core.String key) =>
      saveFieldToDispose(IntSharedPreferencesField(
        key,
        _sharedPreferences,
      ));

  Field<core.Map<core.String, core.dynamic>?> jsonMap(core.String key) =>
      saveFieldToDispose(JsonMapSharedPreferencesField(
        key,
        _sharedPreferences,
      ));

  Field<core.List<core.String>?> stringList(core.String key) =>
      saveFieldToDispose(StringListSharedPreferencesField(
        key,
        _sharedPreferences,
      ));

  Field<core.String?> string(core.String key) =>
      saveFieldToDispose(StringSharedPreferencesField(
        key,
        _sharedPreferences,
      ));

  /// Store instance of [SharedPreferencesField] to dispose when [SharedPreferencesFieldFactory.dispose] is called
  ///
  /// use to store your custom instances of [SharedPreferencesField] created in this factory
  @protected
  Field<T?> saveFieldToDispose<T>(SharedPreferencesField<T> field) {
    _fields.add(field);
    return field;
  }

  /// Closes all saved instances created by this factory
  void dispose() {
    for (final field in _fields) {
      field.dispose();
    }
    _fields.clear();
  }
}