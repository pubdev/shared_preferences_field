import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:shared_preferences_field_delegate/src/utils/object_extension.dart';

import 'iterable_extension.dart';

extension SharedPreferencesExtension on SharedPreferences {
  T? getEnum<T extends Enum>(Iterable<T> values, String key) =>
      getString(key)?.let(
        (it) => values.firstWhereOrNull((selector) => selector.name == it),
      );

  Future<bool> setEnum<T extends Enum>(String key, T value) =>
      setString(key, value.name);

  Map<String, dynamic>? getJson(String key) =>
      getString(key)?.let((it) => jsonDecode(it) as Map<String, dynamic>);

  Future<bool> setJson(String key, Map<String, dynamic>? value) async {
    if (value != null) {
      return setString(key, jsonEncode(value));
    }
    return remove(key);
  }
}
