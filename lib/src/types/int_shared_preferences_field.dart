import 'package:shared_preferences/shared_preferences.dart';

import '../shared_pref.dart';

class IntSharedPreferencesField extends SharedPreferencesField<int> {
  IntSharedPreferencesField(String key, SharedPreferences sharedPreferences)
      : super(
          key,
          sharedPreferences,
          sharedPreferences.getInt,
          sharedPreferences.setInt,
        );
}
