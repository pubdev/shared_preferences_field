import 'package:shared_preferences/shared_preferences.dart';

import '../shared_pref.dart';

class StringListSharedPreferencesField
    extends SharedPreferencesField<List<String>> {
  StringListSharedPreferencesField(
      String key, SharedPreferences sharedPreferences)
      : super(
          key,
          sharedPreferences,
          sharedPreferences.getStringList,
          sharedPreferences.setStringList,
        );
}
