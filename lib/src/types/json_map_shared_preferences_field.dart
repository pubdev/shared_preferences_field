import 'package:shared_preferences/shared_preferences.dart';

import '../shared_pref.dart';
import '../utils/shared_preferences_extension.dart';

class JsonMapSharedPreferencesField
    extends SharedPreferencesField<Map<String, dynamic>> {
  JsonMapSharedPreferencesField(
    String key,
    SharedPreferences sharedPreferences,
  ) : super(
          key,
          sharedPreferences,
          sharedPreferences.getJson,
          sharedPreferences.setJson,
        );
}
