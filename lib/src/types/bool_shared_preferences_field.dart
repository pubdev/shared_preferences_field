import 'package:shared_preferences/shared_preferences.dart';

import '../shared_pref.dart';

class BoolSharedPreferencesField extends SharedPreferencesField<bool> {
  BoolSharedPreferencesField(String key, SharedPreferences sharedPreferences)
      : super(
          key,
          sharedPreferences,
          sharedPreferences.getBool,
          sharedPreferences.setBool,
        );
}
