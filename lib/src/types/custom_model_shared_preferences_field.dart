import 'package:shared_preferences/shared_preferences.dart';

import '../shared_pref.dart';
import '../utils/object_extension.dart';
import '../utils/shared_preferences_extension.dart';

typedef ModelToJsonConverter<T> = Map<String, dynamic> Function(T);
typedef JsonToModelConverter<T> = T Function(Map<String, dynamic>);

/// Used to create [SharedPreferencesField] with custom type
///
/// [toJson] - function that maps [T] to Map<String,dynamic>
/// [fromJson] - function that maps Map<String,dynamic> to [T]
///
/// Example:
/// ```
/// CustomModelSharedPreferencesField<MyModel>(
///   'key',
///   sharedPreferences,
///   (MyModel model) => model.toJson(),
///   (Map<String, dynamic> map) => MyModel.fromJson(map),
/// );
/// ```
class CustomModelSharedPreferencesField<T> extends SharedPreferencesField<T> {
  CustomModelSharedPreferencesField(
    String key,
    SharedPreferences sharedPreferences,
    ModelToJsonConverter<T> toJson,
    JsonToModelConverter<T> fromJson,
  ) : super(
          key,
          sharedPreferences,
          (key) => sharedPreferences.getJson(key)?.let(fromJson),
          (key, value) => sharedPreferences.setJson(key, toJson(value)),
        );
}
