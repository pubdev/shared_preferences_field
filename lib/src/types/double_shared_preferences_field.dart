import 'package:shared_preferences/shared_preferences.dart';

import '../shared_pref.dart';

class DoubleSharedPreferencesField extends SharedPreferencesField<double> {
  DoubleSharedPreferencesField(String key, SharedPreferences sharedPreferences)
      : super(
          key,
          sharedPreferences,
          sharedPreferences.getDouble,
          sharedPreferences.setDouble,
        );
}
