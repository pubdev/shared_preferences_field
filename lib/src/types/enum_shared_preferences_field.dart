import 'package:shared_preferences/shared_preferences.dart';

import '../shared_pref.dart';
import '../utils/shared_preferences_extension.dart';

class EnumSharedPreferencesField<T extends Enum>
    extends SharedPreferencesField<T> {
  EnumSharedPreferencesField(
    String key,
    SharedPreferences sharedPreferences,
    Iterable<T> values,
  ) : super(
          key,
          sharedPreferences,
          (key) => sharedPreferences.getEnum(values, key),
          sharedPreferences.setEnum,
        );
}
