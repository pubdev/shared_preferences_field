import 'package:shared_preferences/shared_preferences.dart';

import '../shared_pref.dart';

class StringSharedPreferencesField extends SharedPreferencesField<String> {
  StringSharedPreferencesField(String key, SharedPreferences sharedPreferences)
      : super(
          key,
          sharedPreferences,
          sharedPreferences.getString,
          sharedPreferences.setString,
        );
}
