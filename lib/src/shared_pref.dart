import 'dart:async';

import 'package:field_delegate/field_delegate.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'shared_preferences_field_factory.dart';

typedef PreferencesGetter<T> = T? Function(String key);
typedef PreferencesSetter<T> = Future<void> Function(String key, T value);

/// Implementation of [Field] for SharedPreferences.
///
/// By default it stores a nullable value but you can make it non-nullable by
/// using [Field.notNullable] method
///
/// There are implementations for different types buy they are not exported
/// excluding [CustomModelSharedPreferencesField] for custom types.
/// It is recommended to use [SharedPreferencesFieldFactory] in order to create
/// instances of this class because it collects created instances to dispose them later.
///
/// if you dont use [SharedPreferencesFieldFactory], you need to call [dispose] manually
///
/// [BoolSharedPreferencesField] for [bool]
/// [DoubleSharedPreferencesField] for [double]
/// [IntSharedPreferencesField] for [int]
/// [StringListSharedPreferencesField] for [List] of [String]
/// [StringSharedPreferencesField] for [String]
/// [BoolSharedPreferencesField] for [bool]
/// [EnumSharedPreferencesField] for [Enum] types
/// [JsonMapSharedPreferencesField] for Map<String,dynamic>
/// and [CustomModelSharedPreferencesField] for custom type or model
abstract class SharedPreferencesField<T> extends Field<T?> {
  late final _streamController = StreamController<T?>.broadcast();
  final String key;
  final SharedPreferences _sharedPreferences;
  final PreferencesGetter<T> _preferencesGetter;
  final PreferencesSetter<T> _preferencesSetter;

  SharedPreferencesField(
    this.key,
    this._sharedPreferences,
    this._preferencesGetter,
    this._preferencesSetter,
  );

  /// Returns a stream of changes of [Field]'s value.
  ///
  /// Emits when [set] method is being called.
  @override
  Stream<T?> get onChanged => _streamController.stream;

  /// Returns [Field]'s value.
  @override
  T? get() => _preferencesGetter(key);

  /// Allows to set a new value to this [Field].
  @override
  Future<void> set(T? value) async {
    _streamController.add(value);
    if (value == null) {
      await _sharedPreferences.remove(key);
    } else {
      await _preferencesSetter(key, value);
    }
  }

  /// Called to close stream of value changes
  void dispose() {
    _streamController.close();
  }
}
