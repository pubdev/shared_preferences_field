## shared_preferences_field_delegate

This package allows to work with shared preferences as fields that have value getter, setter and a stream of changes

This package depends on [field_delegate](https://pub.dev/packages/field_delegate). It is an abstract mutable field delegate that can be used as an interface to access different data sources.
## Usage

To create a `SharedPreferencesField` you need to create a `SharedPreferencesFieldFactory` and call a method with the
desired type and pass the key by which the value can be requested from shared preferences. For example `SharedPreferencesFieldFactory().int('key')`

```dart

final fieldFactory = SharedPreferencesFieldFactory(sharedPreferences);
Field<int?> intField = factory.int(intFieldKey);
int value = intField.get();
Future<void> result = intField.set(1);
Stream<int> changes = intField.onChanged;
```

`SharedPreferencesField` has generic type and by default it is nullable, but you can add a default value to make the
non-nullable `Field`

```dart
Field<int?> intNullableField;
Field<int> notNullableField = Field.notNullable(
  source: nullableFiled,
  defaultValue: 0,
);
```

You can map value of `Field` to other type with `Field.map` function

```dart
Field<int> intField;
Field<String> mappedField = Field.map<int, String>(
  source: intField,
  mapToSource: (value) => int.parse(value),
  mapFromSource: (value) => value.toString(),
);
```

`SharedPreferencesFieldFactory` has a dispose method to cancel the stream of created fields in this factory

```dart

final fieldFactory = SharedPreferencesFieldFactory(sharedPreferences);
Field<int?> intField = factory.int(intFieldKey);
//...
fieldFactory.dispose();
```
