import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences_field_delegate/shared_preferences_field_delegate.dart';

import 'mocks.dart';

main() {
  late SharedPreferencesFieldFactory fieldFactory;
  late StubSharedPreferences sharedPreferences;
  setUp(() {
    sharedPreferences = StubSharedPreferences();
    fieldFactory = SharedPreferencesFieldFactory(sharedPreferences);
  });
  tearDown(() {
    fieldFactory.dispose();
  });
  test('create Field with SharedPreferencesFieldFactory', () async {
    final intField = fieldFactory.int('key');
    expect(intField.get(), null, reason: 'Invalid initial value of Field');
  });
  test('check getter and setter of Field', () {
    final field = fieldFactory.int('key');
    expect(field.get(), null, reason: 'Invalid initial value of Field');
    field.set(2);
    expect(field.get(), 2, reason: 'Invalid value of Field after call set');
  });
  test('check stream of changes of Field', () async {
    final field = fieldFactory.int('key');
    final changesFuture = field.onChanged.take(3).toList();
    await field.set(1);
    await field.set(2);
    await field.set(3);
    final changes = await changesFuture;
    expect(changes, [1, 2, 3], reason: 'invalid list of changes');
  });
  test('check mapping type of Field with Field.map function', () {
    final sourceField = fieldFactory.int('key');
    final mappedField = Field.map<int?, String?>(
      source: sourceField,
      mapToSource: (value) => value != null ? int.tryParse(value) : null,
      mapFromSource: (value) => value?.toString(),
    );
    expect(
      mappedField.get(),
      null,
      reason: 'mapped Field contains invalid value',
    );
    expect(
      sourceField.get(),
      null,
      reason: 'source Field contains invalid value',
    );

    mappedField.set('2');
    expect(
      mappedField.get(),
      '2',
      reason: 'mapped Field contains invalid value',
    );
    expect(
      sourceField.get(),
      2,
      reason: 'source Field contains invalid value',
    );
  });
  test('check creation not nullable Field with Field.notNullable function', () {
    final nullableField = fieldFactory.int('key');
    const defaultValue = 0;
    final notNullableField = Field.notNullable(
      source: nullableField,
      defaultValue: defaultValue,
    );
    expect(
      notNullableField.get(),
      defaultValue,
      reason: 'notNullableField have invalid default value',
    );
    nullableField.set(1);
    expect(
      notNullableField.get(),
      1,
      reason: 'notNullableField have invalid value',
    );
    nullableField.set(null);
    expect(
      notNullableField.get(),
      defaultValue,
      reason:
          'notNullableField have invalid default value after set null to nullableField',
    );
  });
}
